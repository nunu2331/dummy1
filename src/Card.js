import React from 'react';
import { Animated, Dimensions, Easing, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Sepatu from './img/spatu.jpeg';

const {height, width} = Dimensions.get('screen');

const CardComponent = ({
    data, setModalVisible, setSelectedData, setLoading
}) => {

    const visibleModal = (second) => {
        setModalVisible(true);
        setSelectedData({ data });
      };
    
    const buyButton = () => {
        setLoading(true);
    }
    const translateYImage = new Animated.Value(40);

    Animated.timing(translateYImage, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
        easing: Easing.bounce,
    }).start();

    return(
        <View style={styles.container}>
        <Animated.Image
          source={Sepatu}
          resizeMode="contain"
          style={[
            styles.sepatuStyle,
            {
              transform: [
                {
                  translateY: translateYImage,
                },
              ],
            },
          ]}
        />
            <View style={styles.content}>
                <Text style={styles.title}>{data.title}</Text>
                <Text style={styles.nomor}>Barang No.{data.id}</Text>
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity style={styles.buttonStyle} onPress={() => visibleModal()}>
                        <Text style={styles.buttonText}>Details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonStyle} onPress={() => buyButton()}>
                        <Text style={styles.buttonText}>Buy</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        </View>
    )
}

export default CardComponent;

const styles = StyleSheet.create({
    container: {
        height,
        width,
    },
    
    content: {
        flex: 0.4,
        alignItems: 'center',
    },

    sepatuStyle: {
        flex: 0.6,
        width: '100%',
        borderWidth: 1,
    },

    title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333',
        textAlign: 'center',
    },

    nomor: {
        fontSize: 18,
        marginVertical: 12,
        color: '#333',
        textAlign: 'center',
    },

    buttonWrapper: {
        paddingHorizontal: 16,
        marginTop: 12,
        width,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    
    buttonStyle: {
        backgroundColor: '#FF9C35',
        borderRadius: 4,
        paddingHorizontal: 16,
        paddingVertical: 8,
    },

    buttonText: {
        fontWeight: 'bold',
        color: 'white',
    },
});
