import React, { useEffect, useRef, useState } from 'react';
import { Animated, FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CardComponent from './Card';
import Pagination from './Pagination';
import ModalPopUp from './Modal';


const App = ({
    params,
}) => {
const axios = require('axios').default;
const [dataApi, setDataApi] = useState([]);
const [selectedData, setSelectedData] = useState([]);
const [index, setIndex] = useState();
const [modalVisible, setModalVisible] = useState(false);
const [loading, setLoading] = useState(false);
const firstOpacity = useRef(
  new Animated.Value(0)
).current;
const secondOpacity = useRef(
  new Animated.Value(0)
).current;
const thirdOpacity = useRef(
  new Animated.Value(0)
).current;
async function getUser() {
  try {
    const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
    setDataApi(response.data)

  } catch (error) {
    console.error(error);
  }
};


const scrollX = useRef(new Animated.Value(0)).current;
const handleOnScroll = event => {
  Animated.event(
    [
      {
        nativeEvent: {
          contentOffset: {
            x: scrollX,
          },
        },
      },
    ],
    {
      useNativeDriver: false,
    },
  )(event);
};

const handleOnViewableItemsChanged = useRef(({viewableItems}) => {
  setIndex(viewableItems[0].index);
}).current;

const viewabilityConfig = useRef({
  itemVisiblePercentThreshold: 50,
}).current;

useEffect(() => {
  getUser();
}, []);

useEffect(() => {
  if(loading) {

    Animated.stagger(1000, [
      Animated.timing(firstOpacity, {
        toValue: 1,
        useNativeDriver: true,
      }),
      Animated.timing(secondOpacity, {
        toValue: 1,
        useNativeDriver: true,
      }),
      Animated.timing(thirdOpacity, {
        toValue: 1,
        useNativeDriver: true,
      }),
    ]).start()
  }
})
  return(

    <View style={styles.container}>
      {/* <ScrollView> */}

      <FlatList
        data={dataApi}
        horizontal
        pagingEnabled
        snapToAlignment='center'
        showsHorizontalScrollIndicator={false}
        onScroll={handleOnScroll}
        onViewableItemsChanged={handleOnViewableItemsChanged}
        viewabilityConfig={viewabilityConfig}
        renderItem={({item}) => (
          <CardComponent data={item} setLoading={setLoading} setModalVisible={setModalVisible} setSelectedData={setSelectedData}/>
        ) }
        keyExtractor={item => item.id}
      />
      <Pagination data={dataApi} scrollX={scrollX} index={index}/>
      <ModalPopUp data={selectedData} modalVisible={modalVisible} setModalVisible={setModalVisible}/>

{
  loading && 
  <View style={{flexDirection: 'row', position: 'absolute', bottom: 0,flex: 1, display: 'flex'}}>
    <Animated.View
          style={{
            width: 150,
            height: 20,
            backgroundColor: 'orange',
            opacity: firstOpacity,
        }}/>
        <Animated.View
          style={{
            width: 150,
            height: 20,
            backgroundColor: 'orange',
            opacity: secondOpacity,
        }}/>
        <Animated.View
          style={{
            width: 150,
            height: 20,
            backgroundColor: 'orange',
            opacity: thirdOpacity,
        }}/>
  </View>
  
}
    </View>

  )
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
  }
});