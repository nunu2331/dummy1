import React, { useState } from 'react';
import { StyleSheet, Animated, Text, View, Dimensions, TouchableOpacity } from 'react-native';

const{width} = Dimensions.get('screen')

const Pagination = ({
    data, scrollX,
}) => {
    return(
        <View style={styles.container}>
            {data.slice(0, 6).map((_, i) => {
                
                const inputRange = [(i - 1) * width, i * width, (i + 1) * width];

                const dotWidth = scrollX.interpolate({
                inputRange,
                outputRange: [12, 30, 12],
                extrapolate: 'clamp',
                });

                const backgroundColor = scrollX.interpolate({
                inputRange,
                outputRange: ['#ccc', '#000', '#ccc'],
                extrapolate: 'clamp',
                });
                return <Animated.View key={i.toString()} 
                style={[
                  styles.dot,
                  {width: dotWidth, backgroundColor},
                ]} />;
                
           })}
        </View>
    );
};

export default Pagination;
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 35,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },

    dot: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: '#ccc',
        marginHorizontal: 3,
    },
    dotActive: {
      backgroundColor: '#000',
    },
});